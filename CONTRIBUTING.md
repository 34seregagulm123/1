import datetime

import telebot

from telebot import types

#import const

import re

import requests

from .models import *

from AirdropBot.settings import API_TOKEN,ORIGIN

import re

from .choices import type_choices, yes_choices



bot = telebot.TeleBot(API_TOKEN)

botInfo = bot.get_me()

# SECTION_START(HOME): Главная

HOME_MESSAGE = '''

*🏠 Главное меню*

*👛 Актуальные* - актуальные айрдропы

*👛 Мои Айдропы* - список моих айрдропов

*📆 Личный кабинет* - информация о привязанных кошельках

'''



REFERAL_TEMPLATE_ACCOUNT = '''

*👔 Мой профиль*

*🎁 Доступно приятных бонусов*: `{bonuses_available!s}`

*🍎 Приглашено пользователей*: `{invited_count!s}`

Приглашай своих коллег и за каждого вступившего по твоей ссылке получай приятный бонус бесплатно!

'''

REFERAL_TEMPLATE_LINK = '''

*🗣 Реферальная ссылка*:

https://t.me/commentsTGbot?start={magic_number!s}{referal_id!s}

'''



OPERATIONS_ACCOUNT = '''

    *👛 Айрдропы*

*💎 Все актуальные айрдропы*: `{actual_airdrop!s}`

*💳 Мои айрдропы*: `{last_airdrop!s}` - в которых я принимаю/л участие

*📆 Личный кабинет* - привязка кошелька

    '''

ACTIVE_TEXT='Активные айрдропы на данный момент времени:'

HOME_MARKUP =  types.InlineKeyboardMarkup(row_width=1)

HOME_MARKUP.add(

    types.InlineKeyboardButton('👛 Актуальные Айрдропы',callback_data='active'),

    types.InlineKeyboardButton('👛 Мои Айрдропы', callback_data='archive'),

    types.InlineKeyboardButton('📆 Личный кабинет',callback_data='LK')

    )

# Обработчик команды `/start`

@bot.message_handler(commands=['start'])

def command_start(message):

    print("start")

    referal_id = int(message.text[11:] or -1)

    magic_number = int(message.text[6:11] or -1)

    print(referal_id)

    print(magic_number)

    # Проверяем, что это не реферальный id самого пользователя

    if referal_id == message.chat.id: referal_id = None

    # Если пользователь уже зарегистрирован

    try:

        user = User.objects.get(id=message.chat.id)

        GREETING_MESSAGE = 'Приветствую' if not user else 'С возвращением, '

        bot.send_message(message.chat.id, text=GREETING_MESSAGE+user.first_name)

        send_home(message)

    # Иначе регистрируем пользователя

    except User.DoesNotExist:

        user = User.objects.create(

            id=message.from_user.id,

            is_bot=message.from_user.is_bot,

            first_name=message.from_user.first_name,

            last_name=message.from_user.last_name,

            username=message.from_user.username

        )

        airdrop = AirDrop.objects.get(magic_number=magic_number)

        table = Table.objects.create(airdrop=airdrop,user=user)

    #Пробуем найти задание

        if magic_number!=-1:

            user.airdr = magic_number

            user.save()

            print('ok')

            refer=User.objects.get(id=referal_id)

            table2 = Table.objects.get(airdrop=airdrop,user=refer)

            table2.invited_count+=1

            table2.save()

            ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

            ACTION_MARKUP.add(

                types.InlineKeyboardButton('Проверить задачи',callback_data='check'))

            text=airdrop.hello_text

            bot.send_message(message.chat.id,text=text,parse_mode='markdown',reply_markup=ACTION_MARKUP)





# Команда `home` и ключевые фразы `Главное меню` и `Меню`

@bot.message_handler(commands=['home'])

@bot.message_handler(func=lambda message:

    message.text == 'Главное меню' or message.text == 'Меню')

def send_home(message):

    bot.send_message(

        message.chat.id,

        text=HOME_MESSAGE,

        parse_mode='markdown',

        reply_markup=HOME_MARKUP

    )

def send_home2(call):

    bot.send_message(

        call.message.chat.id,

        text=HOME_MESSAGE,

        parse_mode='markdown',

        reply_markup=HOME_MARKUP

    )

#Обработка кнопки 👛 Добавить канал

@bot.message_handler(func=lambda message:

    message.text == '👛 Айрдропы')

def airdrops(message):

    user = User.objects.get(pk=message.chat.id)

    actual_airdrop = Active.objects.all().count()

    archive_airdrop = Table.objects.filter(user=user).count()

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    ACTION_MARKUP.add(

        types.InlineKeyboardButton('Активные айрдропы',callback_data='active'),

        types.InlineKeyboardButton('Мои айрдропы',callback_data='archive'))

    bot.send_message(

        message.chat.id,

        text=OPERATIONS_ACCOUNT.format(actual_airdrop=actual_airdrop, last_airdrop = archive_airdrop),

        parse_mode='markdown',

        reply_markup=ACTION_MARKUP

    )

#АКТИВНЫЕ АЙДРОПЫ-СПИСОК

@bot.callback_query_handler(func=lambda call: call.data == 'active')

def actual_airdrop(call):

    user = User.objects.get(pk=call.message.chat.id)

    actual_airdrops = Active.objects.all()

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    if actual_airdrops.count()!=0:

        for airdrop in actual_airdrops:

                ACTION_MARKUP.add(

                    types.InlineKeyboardButton(str(airdrop.airdrop.name),

                        callback_data='airdrop.id.{}'.format(airdrop.airdrop.id)))

        bot.send_message(

                call.message.chat.id,

                text=ACTIVE_TEXT,

                reply_markup=ACTION_MARKUP

        )

    else:

        bot.send_message(

                call.message.chat.id,

                text='Активных айрдропов еще нет',

                reply_markup=HOME_MARKUP

        )





#АКТИВНЫЕ АЙДРОПЫ-СПИСОК

@bot.callback_query_handler(func=lambda call: call.data == 'archive')

def actual_airdrop(call):

    user = User.objects.get(pk=call.message.chat.id)

    archive_airdrop = Table.objects.filter(user=user)

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    if archive_airdrop.count()!=0:

        for airdrop in archive_airdrop:

                ACTION_MARKUP.add(

                    types.InlineKeyboardButton(str(airdrop.airdrop.name),

                        callback_data='table.id.{}'.format(airdrop.airdrop.id)))



        bot.send_message(

                call.message.chat.id,

                text='Мои айрдропы,в которых я принимаю/л участие',

                reply_markup=ACTION_MARKUP

        )

    else:

        bot.send_message(

                call.message.chat.id,

                text='Моих айрдропов еще нет',

                reply_markup=HOME_MARKUP

        )





@bot.callback_query_handler(func=lambda call: call.data.startswith('table.id.'))

def callback_give_messages(call):

    user=User.objects.get(pk=call.message.chat.id)

    print(int(call.data[9:]))

    flag=int(call.data[9:])

    user.flag_airdrop=flag

    airdrop=AirDrop.objects.get(pk=user.flag_airdrop)

    table = Table.objects.get(user=user,airdrop=airdrop)

    user.save()

    amount = airdrop.users.count()

    AirDrop_TEXT = '*Название:* '+str(airdrop.name)+'\n*Дата начала:* '+str(airdrop.start_date)+\

                    '\n*Дата окончания:* '+str(airdrop.end_date)+'\n*Пул розыгрыша:* '+str(airdrop.cost)

    if airdrop.type==2:

        AirDrop_TEXT=AirDrop_TEXT+'\n*Процентная ставка выплат:* '+str(airdrop.percent)+' %'

    if airdrop.type==3:

        AirDrop_TEXT=AirDrop_TEXT+'*Мах участников:* '+str(airdrop.amount_fix)

    AirDrop_TEXT+='\n*Вы заработали* - ' + str(table.points)+' балла'+\

    '\n*Вы пригласили в рамках конкурса людей* - '+str(table.invited_count)+'\n*За айдроп ваша награда* - '+str(table.cash)

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    ACTION_MARKUP.add(

        types.InlineKeyboardButton('Назад',callback_data='archive'),

            types.InlineKeyboardButton('Меню',callback_data='back'))

    bot.send_message(

        call.message.chat.id,

        text=AirDrop_TEXT,

        parse_mode='markdown',

        reply_markup=ACTION_MARKUP

    )

@bot.callback_query_handler(func=lambda call: call.data.startswith('airdrop.id.'))

def callback_give_messages(call):

    user=User.objects.get(pk=call.message.chat.id)

    flag=int(call.data[11:])

    user.flag_airdrop=flag

    airdrop = AirDrop.objects.get(pk=user.flag_airdrop)

    user.save()

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    ACTION_MARKUP.add(

        types.InlineKeyboardButton('Проверить задачи',callback_data='check'),

        types.InlineKeyboardButton('Меню',callback_data='back'))

    text=airdrop.hello_text

    bot.send_message(call.message.chat.id,text=text,parse_mode='markdown',reply_markup=ACTION_MARKUP)

@bot.callback_query_handler(func=lambda call: call.data=='work')

def callback_give_messages(call):

    user=User.objects.get(pk=call.message.chat.id)

    airdrop = AirDrop.objects.get(pk=user.flag_airdrop)

    if airdrop.type == 3 and (1+airdrop.amount)>airdrop.amount_fix:

        print(1+airdrop.amount)

        bot.send_message(message.chat.id,text='К сожалению, кол-во участников уже превысит заявленное. Но скоро будут и другие айрдропы.')

        airdrop.full==1

        airdrop.save()

    else:

        text = '\n'

        for work in airdrop.works.all():

            text=text+'\n'+str(work.name)+' '+str(work.link)+\

                '\nВы получите за это задание: '+str(work.points)+' баллов'

        ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

        ACTION_MARKUP.add(

            types.InlineKeyboardButton('Проверить задачи',callback_data='check'),

                types.InlineKeyboardButton('Меню',callback_data='back'))

        bot.send_message(

            call.message.chat.id,

            text=text,

            reply_markup=ACTION_MARKUP

        )

#проверка выполнения этапов айрдропа

@bot.callback_query_handler(func=lambda call: call.data=='check')

def callback_give_messages(call):

    user=User.objects.get(pk=call.message.chat.id)

    try:

        airdrop = AirDrop.objects.get(pk=user.flag_airdrop)

    except:

        airdrop = AirDrop.objects.get(magic_number=user.airdr)

    text=' '

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    ACTION_MARKUP.add(

        types.InlineKeyboardButton('Обновить',callback_data='check'),

        types.InlineKeyboardButton('Меню',callback_data='back')

        )

    points=0

    done = 0

    amount=airdrop.works.all().count()

    print(amount)

    for  work in airdrop.works.all():

        if work.number!=None:

            if bot.get_chat_member(

                chat_id='@'+work.number,

                user_id=call.message.chat.id

            ).status in ['creator', 'administrator', 'member']:

                airdrop.users.add(user)

                points+=work.points

                print('pl')

                airdrop.amount=airdrop.users.count()

                airdrop.save()

                done+=1

            else:

                 send="""\nНажмите кнопку ниже, чтобы """+str(work.name)

                 ACT_MARKUP = types.InlineKeyboardMarkup(row_width=1)

                 ACT_MARKUP.add(

                    types.InlineKeyboardButton(str(work.name),url='https://t.me/'+str(work.number)))

                 bot.send_message(

                            call.message.chat.id,

                            text=send,

                            reply_markup=ACT_MARKUP

                    )

    text="""

Баланс

Очки: {points!s}

    """

    if airdrop.magic_number!=0:

        if (done+1)==amount:

            try:

                table = Table.objects.get(airdrop=airdrop,user=user)

            except Table.DoesNotExist:

                table = Table.objects.create(

                    user=user,

                    airdrop=airdrop)

            ref='\nКоличество рефов - '+str(table.invited_count)+REFERAL_TEMPLATE_LINK

            text+=ref

            bot.send_message(

                        call.message.chat.id,

                        text=text.format(magic_number=airdrop.magic_number,\

                            points=points, referal_id=call.message.chat.id),

                            reply_markup=ACTION_MARKUP)

        else:

            ACTI_MARKUP = types.InlineKeyboardMarkup(row_width=1)

            ACTI_MARKUP.add(

               types.InlineKeyboardButton('Проверить задачи',callback_data='check'))

            bot.send_message(

                         call.message.chat.id,

                         text='Вы выполнили не все задачи',parse_mode='markdown',

                             reply_markup=ACTI_MARKUP)

    elif done==amount:

        try:

            table = Table.objects.get(airdrop=airdrop,user=user)

        except Table.DoesNotExist:

            table = Table.objects.create(

                user=user,

                airdrop=airdrop)

        bot.send_message(

                    call.message.chat.id,

                    text=text.format(points=points),

                        reply_markup=ACTION_MARKUP)

    else:

        ACTI_MARKUP = types.InlineKeyboardMarkup(row_width=1)

        ACTI_MARKUP.add(

           types.InlineKeyboardButton('Проверить задачи',callback_data='check'))

        bot.send_message(

                     call.message.chat.id,

                     text=text,

                         reply_markup=ACTI_MARKUP)



#Обработка кнопки 📆 Личный кабинет

@bot.callback_query_handler(func=lambda call: call.data == 'LK')

def LK(call):

    LK_ACCOUNT = '''

    *📆 Личный кабинет*

*💎 Привязать TRX-кошелек*

    '''

    user = User.objects.get(pk=call.message.chat.id)

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    ACTION_MARKUP.add(

                types.InlineKeyboardButton('💎Привязать кошелек',

                    callback_data='wallet'),

                types.InlineKeyboardButton('💳Мой кошелек',

                    callback_data='mywallets'),

                        types.InlineKeyboardButton('Меню',callback_data='back'))

    bot.send_message(

        call.message.chat.id,text=LK_ACCOUNT,parse_mode='markdown',

        reply_markup=ACTION_MARKUP

    )



@bot.callback_query_handler(func=lambda call: call.data == 'mywallets')

def edit_wallet(call):

    user=User.objects.get(id=call.message.chat.id)

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    if user.wallet !=None:

        ALL='\nПлатежная система TRON'+\

            '\nНомер кошелька: '+ str( user.wallet)

        ACTION_MARKUP.add(

               types.InlineKeyboardButton('Изменить номер',callback_data='edit'),

               types.InlineKeyboardButton('Удалить',callback_data='delete'),

                   types.InlineKeyboardButton('Меню',callback_data='back'))

        bot.send_message(call.message.chat.id,text=ALL,reply_markup=ACTION_MARKUP)

    else:

        ACTION_MARKUP.add(

            types.InlineKeyboardButton('Привязать кошельки', callback_data='wallet'))

        bot.send_message(call.message.chat.id, text='У вас еще нет привязанных кошельков',reply_markup = ACTION_MARKUP)



@bot.callback_query_handler(func=lambda call: call.data == 'wallet')

def new_wallet(call):

    user=User.objects.get(id=call.message.chat.id)

    if user.wallet!=None:

        ALL= '\nУ вас уже есть кошелек для платежной системы TRON'+\

            '\n Номер кошелька: '+ str(user.wallet)+'\n Вы можете изменить номер кошелька/удалить кошелек\n'

        ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

        ACTION_MARKUP.add(

           types.InlineKeyboardButton('Изменить номер',callback_data='edit'),

           types.InlineKeyboardButton('Удалить',callback_data='delete'),

               types.InlineKeyboardButton('Меню',callback_data='back'))

        bot.send_message(call.message.chat.id,text=ALL,reply_markup=ACTION_MARKUP)

    else:

        bot.register_next_step_handler(

                message=bot.send_message(

                    chat_id=call.message.chat.id,

                    text=user.first_name+''', отлично пришлите в чат номер своего кошелька для платежной системы TRON'''

                ),

                callback=command_edit_wallet

            )





def command_edit_wallet(message, invite=False):

    bot.clear_step_handler(message)

        # Переспрашиваем, если пользователь отправил пустое сообщение (например только из пробелов)

    if not message.text.strip():

         bot.register_next_step_handler(

                message=bot.send_message(

                    chat_id=message.chat.id,

                    text='Пришлите номер своего кошелька для платежной системы'

                ),

                callback=command_edit_wallet

            )

         return

        # Создаем объект кошелька

    user=User.objects.get(id=message.chat.id)

    number = str(''.join(str(i) for i in message.text.split()))

    user.wallet=number

    user.save()

    bot.send_message(message.chat.id,text='Кошелек для платежной системы TRON создан.')

    send_home(message)



@bot.callback_query_handler(func=lambda call: call.data == 'edit')

def edit_old_wallet(call):

    user=User.objects.get(pk=call.message.chat.id)

    bot.register_next_step_handler(

                message=bot.send_message(

                    chat_id=call.message.chat.id,

                    text=user.first_name+''', отлично пришли в чат новый номер своего кошелька для платежной системы TRON '''

                ),

                callback=command_edit_old_wallet

            )

def command_edit_old_wallet(message, invite=False):

    bot.clear_step_handler(message)

    # Переспрашиваем, если пользователь отправил пустое сообщение (например только из пробелов)

    if not message.text.strip():

     bot.register_next_step_handler(

            message=bot.send_message(

                chat_id=message.chat.id,

                text='Пришли новый номер своего кошелька для платежной системы TRON'

            ),

            callback=command_edit_wallet

        )

     return

    # Редачим объект кошелька

    user=User.objects.get(id=message.chat.id)

    number = str(''.join(str(i) for i in message.text.split()))

    user.wallet=number

    user.save()

    bot.send_message(message.chat.id,text='Кошелек для платежной системы TRON изменен.')

    send_home(message)



@bot.callback_query_handler(func=lambda call: call.data == 'delete')

def question_old_wallet(call):

    user=User.objects.get(pk=call.message.chat.id)

    ACTION_MARKUP = types.InlineKeyboardMarkup(row_width=1)

    ACTION_MARKUP.add(

           types.InlineKeyboardButton('Да', callback_data='yes'),

           types.InlineKeyboardButton('Нет', callback_data='no'),

               types.InlineKeyboardButton('Меню',callback_data='back')

           )

    bot.send_message(call.message.chat.id,text='Вы точно хотите удалить кошелек для платежной системы TRON ?',reply_markup=ACTION_MARKUP)



@bot.callback_query_handler(func=lambda call: call.data == 'yes')

def delete_old_wallet(call):

    user=User.objects.get(pk=call.message.chat.id)

    user.wallet=None

    user.save()

    bot.send_message(call.message.chat.id,text='Ваш кошелек для платежной системы TRON удален')

    send_home(call.message)



@bot.callback_query_handler(func=lambda call: call.data == 'no')

def old_wallet(call):

    edit_wallet(call)



@bot.callback_query_handler(func=lambda call: call.data == 'back')

def back(call):

    send_home2(call)







# Отправляем начальный экран на любое неизвестное сообщение

@bot.message_handler(func=lambda message: message.chat.type == 'private')

def echo_all(message):

    send_home(message)